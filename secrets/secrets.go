package secrets

import (
	"context"
	"fmt"
	"os"
	"strings"

	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
	"github.com/joho/godotenv"
)

const DefaultSecretEnvName = "SECRET_ENV"

// if envName is empty will use DefaultSecretEnvName
func ApplyFromEnv(ctx context.Context, envName string) error {
	if envName == "" {
		envName = DefaultSecretEnvName
	}
	secretManagerURL, ok := os.LookupEnv(envName)
	if !ok {
		return fmt.Errorf("env not set: %q", envName)
	}
	secretContents, err := Get(ctx, secretManagerURL)
	if err != nil {
		return err
	}
	em, err := BytesToEnvMap(secretContents)
	if err != nil {
		return err
	}
	em.Apply(false)
	return nil
}

func BytesToEnvMap(b []byte) (envMap EnvMap, err error) {
	return godotenv.UnmarshalBytes(b)
}

type EnvMap map[string]string

func (em EnvMap) Apply(overload bool) {
	// Make list of current env variables set
	envVarsSet := map[string]bool{}
	currentEnv := os.Environ()
	for _, rawEnvLine := range currentEnv {
		key := strings.Split(rawEnvLine, "=")[0]
		envVarsSet[key] = true
	}

	// Write variables from secrets if not already set
	for key, value := range em {
		if !envVarsSet[key] || overload {
			_ = os.Setenv(key, value)
		}
	}

}

func Get(ctx context.Context, u string) ([]byte, error) {
	c, err := secretmanager.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	req := &secretmanagerpb.AccessSecretVersionRequest{
		Name: u,
	}
	result, err := c.AccessSecretVersion(ctx, req)
	if err != nil {
		return nil, err
	}
	if result != nil {
		if result.Payload != nil {
			return result.Payload.Data, nil
		}
	}
	return nil, fmt.Errorf("Failed to secure the secret %q", u)
}
