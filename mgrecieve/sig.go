package mgrecieve

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/hex"
	"io"
	"log/slog"
	"net/http"
	"os"
)

const EnvName = "MAILGUN_WEBHOOK_SIGNING_KEY"

// Find in https://app.mailgun.com/settings/api_security
type HTTPWebhookSigningKey []byte

func FromEnv() HTTPWebhookSigningKey {
	return HTTPWebhookSigningKey(os.Getenv(EnvName))
}

// VerifyWebhookRequest
func (ac HTTPWebhookSigningKey) VerifyWebhookRequest(req *http.Request) (verified bool, err error) {
	h := hmac.New(sha256.New, ac)
	_, _ = io.WriteString(h, req.FormValue("timestamp"))
	_, _ = io.WriteString(h, req.FormValue("token"))

	calculatedSignature := h.Sum(nil)
	signature, err := hex.DecodeString(req.FormValue("signature"))
	if err != nil {
		return false, err
	}
	if len(calculatedSignature) != len(signature) {
		return false, nil
	}

	return subtle.ConstantTimeCompare(signature, calculatedSignature) == 1, nil
}

func (ac HTTPWebhookSigningKey) MustBeFromMailgun(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		verified, err := ac.VerifyWebhookRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			slog.Error("Problem verifying signature call from mailgun", "err", err, "request", r)
			return
		}
		if !verified {
			w.WriteHeader(http.StatusForbidden)
			slog.Info("not a verified mailgun forward", "request", r)
			return
		}
		h.ServeHTTP(w, r)
	})
}
