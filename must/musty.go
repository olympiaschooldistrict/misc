package must

import (
	"fmt"
	"os"
)

func Fatal[T any](t T, err error) T {
	FatalIfErr(err)
	return t
}

func FatalIfErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func Panic[T any](t T, err error) T {
	PanicIfErr(err)
	return t
}

func PanicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
