package bump

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/olympiaschooldistrict/misc/git"
	"bitbucket.org/olympiaschooldistrict/misc/semver"
)

var (
	Major = bumper(majorBump)
	Minor = bumper(minorBump)
	Patch = bumper(patchBump)
)

func InitialSemver(remote string) (err error) {
	remote = defaultRemoteIfNecessary(remote)
	largestTag, _ := getTag()
	if largestTag != "" {
		return fmt.Errorf("There's already semver tags")
	}
	newTag := "v0.0.0"

	err = git.MakeTag(newTag)
	if err != nil {
		return fmt.Errorf("I'm bad at tagging? \n%v\n", err)
	}
	err = git.PushTagToRemote(newTag, remote)
	if err != nil {
		return fmt.Errorf("I had problems pushing the tag: %v\n", err)
	}
	return
}

func bumper(bf func(sv string) string) func(remote string) error {
	return func(remote string) (err error) {
		remote = defaultRemoteIfNecessary(remote)
		largestTag, err := getTag()
		if err != nil {
			return err
		}

		newTag := bf(largestTag)

		err = git.MakeTag(newTag)
		if err != nil {
			return fmt.Errorf("I'm bad at tagging? \n%v\n", err)
		}
		err = git.PushTagToRemote(newTag, remote)
		if err != nil {
			return fmt.Errorf("I had problems pushing the tag: %v\n", err)
		}
		return
	}
}

func majorBump(largestTag string) string {
	major, _ := strconv.Atoi(strings.TrimPrefix(semver.Major(largestTag), "v"))
	major++
	return fmt.Sprintf("v%d.0.0", major)
}
func minorBump(largestTag string) string {
	minor, _ := strconv.Atoi(semver.Minor(largestTag))
	minor++
	return fmt.Sprintf("%s.%v.0", semver.Major(largestTag), minor)
}
func patchBump(largestTag string) string {
	patch, _ := strconv.Atoi(semver.Patch(largestTag))
	patch++
	return fmt.Sprintf("%s.%v", semver.MajorMinor(largestTag), patch)
}
func initial(largestTag string) string {
	return "v0.0.0"
}

func defaultRemoteIfNecessary(s string) string {
	if s == "" {
		return "origin"
	}
	return s
}

func getTag() (string, error) {
	myTag, _ := git.CurrentTag()
	if myTag != "" {
		return "", fmt.Errorf("You have already tagged this commit")
	}
	largestTag := git.LargestTag()
	if largestTag == "" {
		return "", ErrNoSemverTags
	}
	return largestTag, nil
}

var ErrNoSemverTags = fmt.Errorf("You haven't ever used a semver verion in this repo")
