package main // import "bitbucket.org/olympiaschooldistrict/deployversion"

import (
	"fmt"
	"os"
	"strings"

	"bitbucket.org/olympiaschooldistrict/misc/git"
)

func main() {
	var err error
	if !git.NotModified() {
		fmt.Fprintf(os.Stderr, "There are modified files that need to be committed\n")
		os.Exit(1)
	}
	if git.UntrackedFiles() {
		fmt.Fprintf(os.Stderr, "There are untracked files that need to be committed or ignored\n")
		os.Exit(2)
	}
	tag, err := git.CurrentTag()
	if err != nil {
		fmt.Fprintf(os.Stderr, "The current commit is not tagged: %v\n", err)
		os.Exit(3)
	}
	fmt.Fprintf(os.Stdout, tagReplace(string(tag)))
}

func tagReplace(s string) string {
	return strings.ReplaceAll(strings.ToLower(s), ".", "-")
}
