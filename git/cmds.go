package git

import (
	"bytes"
	"errors"
	"fmt"
	"os/exec"
	"sort"
	"strings"

	"bitbucket.org/olympiaschooldistrict/misc/semver"
)

var ErrSemverNotValid = errors.New(`Not a valid Semver version`)

func AllTags() (tags []string, err error) {
	cmd := exec.Command("git", "tag", "-l", "v[0-9]*.*.*")
	out, err := cmd.Output()
	if err != nil {
		return
	}
	btags := bytes.Split(out, []byte("\n"))
	for _, tag := range btags {
		if semver.IsValid(string(tag)) {
			tags = append(tags, string(tag))
		}
	}
	sort.Slice(tags, func(i, j int) bool { return semver.Compare(tags[i], tags[j]) == 1 })

	return
}

func LargestTag() string {
	tags, err := AllTags()
	if err != nil {
		return ""
	}
	return tags[0]
}

func LastFirstParentTag() string {
	for i := 0; ; i++ {
		cmd := exec.Command("git", "tag", "--points-at", lastNCommit(i))
		btag, err := cmd.Output()
		if err != nil {
			return ""
		}
		// fmt.Printf("%v: %q\n", i, string(btag))
		tag := strings.TrimSpace(string(btag))
		if semver.IsValid(tag) {
			return tag
		}
	}
}

// this only traverses the first parent
func lastNCommit(n int) string {
	if n <= 0 {
		return "HEAD"
	}
	return fmt.Sprintf("HEAD~%d", n)
}

func NotModified() bool {
	cmd := exec.Command("git", "diff", "--exit-code")
	return cmd.Run() == nil
}

func UntrackedFiles() bool {
	cmd := exec.Command("git", "diff", "--exit-code", "--cached")
	err := cmd.Run()
	return err != nil
}

func CurrentTag() (string, error) {
	cmd := exec.Command("git", "tag", "--points-at", "HEAD")
	btag, err := cmd.Output()
	if err != nil {
		return "", err
	}
	tag := strings.TrimSpace(string(btag))
	if semver.IsValid(tag) {
		return tag, nil
	}
	return "", ErrSemverNotValid
}

func MakeTag(newTag string) (err error) {
	cmd := exec.Command("git", "tag", newTag)
	errbuf := &bytes.Buffer{}
	cmd.Stderr = errbuf
	_, err = cmd.Output()
	if err != nil {
		return fmt.Errorf("Problems Making tag: %v\nStderr\n%s\n\n", err, errbuf.String())
	}
	return nil
}

func PushTagToRemote(newTag, remoteName string) (err error) {
	cmd := exec.Command("git", "push", remoteName, newTag)
	errbuf := &bytes.Buffer{}
	cmd.Stderr = errbuf
	_, err = cmd.Output()
	if err != nil {
		return fmt.Errorf("Problems pushing the tag: %v\nStderr\n%s\n\n", err, errbuf.String())
	}
	return nil
}
