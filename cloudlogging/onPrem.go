package cloudlogging

import (
	"encoding/json"
	"fmt"

	"cloud.google.com/go/logging"
	mrpb "google.golang.org/genproto/googleapis/api/monitoredres"
)

// Example of how you might want to set up an OnPremService to write to a cloud stderr log
//
//  import (
//		"context"
//		"bitbucket.org/olympiaschooldistrict/misc/cloudlogging"
//  )
//
//  func setDefaultSlogToMirrorCloudLoggingAndLocalStderr(ctx context.Context, project string) (*OnPremWriter, error) {
//  	loggingClient, err := logging.NewClient(ctx, project)
//  	if err != nil {
//  		return nil, err
//  	}
//  	err = loggingClient.Ping(ctx)
//  	if err != nil {
//  		return nil, err
//  	}
//  	stderrLog := loggingClient.Logger("stderr")
//  	opw := NewOnPremWriter(stderrLog)
//  	slog.SetDefault(
//  		slog.New(
//  			cloudlogging.NewHandler(
//  				io.MultiWriter(opw, os.Stderr),
//  			),
//  		),
//  	)
//  	return opw, nil
//  }

// NewOnPremWriter instantiates a writer that pushes the written bytes to a cloud logging log (in a nonsyncronous way),
// including default monitored resources and labels for convenience and translating slog levels to Google Cloud Logging ones
func NewOnPremWriter(c *logging.Logger) *OnPremWriter {
	return &OnPremWriter{loggingLogger: c}
}

type OnPremWriter struct {
	loggingLogger   *logging.Logger
	DefaultLabels   map[string]string
	DefaultResource *mrpb.MonitoredResource
}

func (lw *OnPremWriter) Write(b []byte) (int, error) {
	var payload any

	// if the bytes we are passed are json, pass them to payload as raw bytes, else pass the payload as a string
	err := json.Unmarshal(b, &payload)
	if err != nil {
		fmt.Println(err.Error())
		payload = string(b)
	}

	le := logging.Entry{
		Resource: lw.DefaultResource,
		Labels:   lw.DefaultLabels,
		Payload:  payload,
	}

	pr, ok := payload.(map[string]any)
	if ok {
		switch pr["level"] {
		case "DEBUG":
			le.Severity = logging.Debug
		case "INFO":
			le.Severity = logging.Info
		case "WARN":
			le.Severity = logging.Warning
		case "ERROR":
			le.Severity = logging.Error
		default:
			le.Severity = logging.Default
		}
	}

	lw.loggingLogger.Log(le)
	return len(b), nil
}
