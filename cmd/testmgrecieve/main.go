package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/olympiaschooldistrict/misc/mgrecieve"
)

const DefaultPort = "8080"

func main() {
	mgkey := mgrecieve.FromEnv()
	http.Handle("/", mgkey.MustBeFromMailgun(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "We're good.")
	})))
	port := os.Getenv("PORT")
	if port == "" {
		port = DefaultPort
	}
	fmt.Println("Listening on http://localhost:" + port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatalln(err)
	}
	return
}
