package main

import (
	"log"
	"os"
	"os/exec"

	"github.com/urfave/cli/v2"

	"bitbucket.org/olympiaschooldistrict/misc/secrets"
)

func main() {
	err := (&cli.App{
		Name:  "injectme",
		Usage: "Inject Secrets from google",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "force",
				Usage:   "overwrite all env vars defined",
				Aliases: []string{"f"},
			},
			&cli.StringFlag{
				Name:        "SecretURL",
				DefaultText: "",
				Usage:       "the url of the secret to inject",
				Required:    true,
				EnvVars:     []string{"SECRET_ENV", "SECRET_URL"},
			},
		},
		Action: func(ctx *cli.Context) error {
			if ctx.NArg() == 0 {
				return cli.Exit("Not much point in injecting variables if you aren't gonna run anything", 2)
			}
			secretBytes, err := secrets.Get(ctx.Context, ctx.String("SecretURL"))
			if err != nil {
				return err
			}
			em, err := secrets.BytesToEnvMap(secretBytes)
			if err != nil {
				return err
			}
			em.Apply(ctx.Bool("force"))

			cmd := exec.Command(ctx.Args().First(), ctx.Args().Tail()...)
			// fmt.Println(cmd.Environ())
			cmd.Stderr = os.Stderr
			cmd.Stdout = os.Stdout
			cmd.Stdin = os.Stdin

			return cmd.Run()
		},
	}).Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}
