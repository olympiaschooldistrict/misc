package main

import "bitbucket.org/olympiaschooldistrict/misc/bump"

func Inital() error {
	return bump.InitialSemver("")
}
func BumpPatch() error {
	return bump.Patch("")
}
func BumpMinor() error {
	return bump.Minor("")
}
func BumpMajor() error {
	return bump.Major("")
}
