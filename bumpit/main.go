package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/olympiaschooldistrict/misc/git"
	"bitbucket.org/olympiaschooldistrict/misc/semver"
)

// we assume patch version is desired
var (
	minor  = flag.Bool("minor", false, "bump the minor version (default is patch)")
	remote = flag.String("remote", "origin", "Name of remote repo to push tag to")
)

func main() {
	flag.Parse()
	myTag, _ := git.CurrentTag()
	if myTag != "" {
		fmt.Fprintln(os.Stderr, "You have already tagged this commit")
		os.Exit(1)
		return
	}
	largestTag := git.LargestTag()
	if largestTag == "" {
		fmt.Fprintln(os.Stderr, "You haven't ever used a semver verion in this repo")
		os.Exit(1)
		return
	}
	var newTag string

	if *minor {
		minor, _ := strconv.Atoi(semver.Minor(largestTag))
		minor++
		newTag = fmt.Sprintf("%s.%v.0", semver.Major(largestTag), minor)
	} else {
		patch, _ := strconv.Atoi(semver.Patch(largestTag))
		patch++
		newTag = fmt.Sprintf("%s.%v", semver.MajorMinor(largestTag), patch)
	}
	err := git.MakeTag(newTag)
	if err != nil {
		fmt.Fprintf(os.Stderr, "I'm bad at tagging? \n%v\n", err)
		os.Exit(1)
		return
	}
	err = git.PushTagToRemote(newTag, *remote)
	if err != nil {
		fmt.Fprintf(os.Stderr, "I had problems pushing the tag: %v\n", err)
		os.Exit(1)
		return
	}
	return
}
